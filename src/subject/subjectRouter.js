const subjectController = require('./subjectController')
const express = require('express');
const router = express.Router();

router.get('/', subjectController.getAllSubjects);
router.get('/:code', subjectController.getSubjectByCode);
router.post('/',subjectController.insertSubject);
router.put('/:code', subjectController.updateSubjectByCode);
router.delete('/:code', subjectController.deleteSubjectByCode);

module.exports = router;