const db = require('./../services/database');

class SubjectModel {
    static getAllSubjects = (callback) => {
        const query = "SELECT * FROM subject";

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static getSubjectByCode = (code, callback) => {
        const query = `SELECT * FROM subject WHERE code =${code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static insertSubject = (subject, callback) => {
        const query = `INSERT INTO subject (name,credits,major_code)
        VALUES ("${subject.name}",${subject.credits},${subject.majorCode})`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static deleteSubjectByCode = (code, callback) => {
        const query = `DELETE FROM subject WHERE code=${code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static updateSubject = (subject, callback) => {
        const query = `UPDATE subject SET name="${subject.name}",
            credits=${subject.credits},major_code =${subject.majorCode}
            WHERE code = ${subject.code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };
}

module.exports = SubjectModel;