const SubjectModel = require('./subject');

const getAllSubjects = (req, res) => {
    SubjectModel.getAllSubjects((error, results) => {
        const responseData = {};

        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = error;
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["data"] = results;
        }

        res.send(JSON.stringify(responseData));
    });
};

const getSubjectByCode = (req, res) => {
    const responseData = {};

    const { code } = req.params;
    // validate request param
    if (code == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(code)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    }

    SubjectModel.getSubjectByCode(code, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "Invalid code";
        } else {
            if (results.length) {
                res.statusCode = 200;
                responseData["status"] = "Success";
                responseData["data"] = results;
            } else {
                res.statusCode = 404;
                responseData["status"] = "Failed";
                responseData["message"] = "data not found"
            }
        }

        res.send(JSON.stringify(responseData));
    });
};

const insertSubject = (req, res) => {
    const responseData = {};

    const { name, credits, major_code: majorCode } = req.body;

    const subject = {
        "name": name, "credits": credits, "majorCode": majorCode
    };

    SubjectModel.insertSubject(subject, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "insert subject failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["data"] = "subject inserted successfully";
        }

        res.send(JSON.stringify(responseData));
    });
};
const updateSubjectByCode = (req, res) => {
    const responseData = {};

    const { code } = req.params;
    const { name, credits, major_code: majorCode } = req.body;
    const subject = {
        "code": code, "name": name, "credits": credits, "majorCode": majorCode
    };

    SubjectModel.updateSubject(subject, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "update subject failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "subject updated successfully";
        }

        res.send(JSON.stringify(responseData));
    });
};

const deleteSubjectByCode = (req, res) => {
    const responseData = {};

    const { code } = req.params;
    // validate request param
    if (code == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(code)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    }


    SubjectModel.deleteSubjectByCode(code, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "Delete subject failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "Subject deleted successfully";
        }

        res.send(JSON.stringify(responseData));
    });
};

module.exports = { getAllSubjects, updateSubjectByCode, getSubjectByCode, deleteSubjectByCode, insertSubject };