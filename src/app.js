const express = require('express');
const bodyParser = require('body-parser');
const db = require('./services/database');

const app = express();
const port = process.env.PORT || 3000;

// import all router
const studentRoute = require('./student/studentRouter');
const majorRoute = require('./major/majorRouter');
const subjectRoute = require('./subject/subjectRouter');
const studyPlanRoute = require('./studyplan/studyPlanRouter');

// add body parser on json
app.use(bodyParser.json());

// register router
app.use('/students', studentRoute);
app.use('/majors', majorRoute);
app.use('/subjects', subjectRoute);
app.use('/study-plans', studyPlanRoute);

// run server
app.listen(port, () => {
    db.connect(function (err) {
        if (err) throw err;
        console.log("Database connected");
    });
    console.log(`Server running at http://localhost:${port}`);
});
