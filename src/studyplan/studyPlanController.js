const StudyPlanModel = require('./studyPlan');

const getAllStudyPlans = (req, res) => {
    StudyPlanModel.getAllStudyPlans((error, results) => {
        const responseData = {};

        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = error;
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["data"] = results;
        }

        res.send(JSON.stringify(responseData));
    });
};

const insertStudyPlan = (req, res) => {
    const responseData = {};

    const { student_nim: studentNim, subject_code: subjectCode } = req.body;

    const studyPlan = {
        "studentNim": studentNim,
        "subjectCode": subjectCode
    };

    StudyPlanModel.insertStudyPlan(studyPlan, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "insert studyplan failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "studyplan inserted successfully";
        }

        res.send(JSON.stringify(responseData));
    });
};

const updateStudyPlan = (req, res) => {
    const responseData = {};

    const {code} = req.params;

    const {student_nim: studentNim, subject_code: subjectCode } = req.body;

    const studyPlan = {
        "code": code,
        "studentNim": studentNim,
        "subjectCode": subjectCode
    };

    StudyPlanModel.updateStudyPlan(studyPlan, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "update studyplan failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "studyplan updated successfully";
        }
        res.send(JSON.stringify(responseData));
    });
};

const getStudyPlan = (req, res) => {
    const responseData = {};

    const { code } = req.params;
    // validate request param
    if (code == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(code)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    }

    StudyPlanModel.getStudyPlanByCode(code, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "error while retrieving data";
        } else {
            if (results.length) {
                res.statusCode = 200;
                responseData["status"] = "Success";
                responseData["data"] = results;
            } else {
                res.statusCode = 404;
                responseData["status"] = "Failed";
                responseData["message"] = "data not found"
            }
        }
        res.send(JSON.stringify(responseData));
    });
};

const deleteStudyPlan = (req, res) => {
    const responseData = {};

    const { code } = req.params;
    // validate request param
    if (code == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed",
            responseData["message"] = "Missing parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(code)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter: code";
        res.send(JSON.stringify(responseData));
        return;
    }

    StudyPlanModel.deleteStudyPlanByCode(code, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "delete studyplan failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "studyplan deleted successfully";
        }
        res.send(JSON.stringify(responseData));
    });
};

module.exports = { getAllStudyPlans, updateStudyPlan, getStudyPlan, deleteStudyPlan, insertStudyPlan };