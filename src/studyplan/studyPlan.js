const db = require('./../services/database');

class StudyPlanModel {

    static getAllStudyPlans = (callback) => {
        const query = "SELECT * FROM studyplan";

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static getStudyPlanByCode = (code, callback) => {
        const query = `SELECT * FROM studyplan WHERE code=${code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static insertStudyPlan = (studyPlan, callback) => {
        const query = `INSERT INTO studyplan (student_nim,subject_code) 
            VALUES (${studyPlan.studentNim},${studyPlan.subjectCode})`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static deleteStudyPlanByCode = (code, callback) => {
        const query = `DELETE FROM studyplan WHERE code=${code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static updateStudyPlan = (studyPlan, callback) => {
        const query = `UPDATE studyplan SET student_nim=${studyPlan.studentNim},
            subject_code=${studyPlan.subjectCode} WHERE code=${studyPlan.code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    }
}

module.exports = StudyPlanModel;