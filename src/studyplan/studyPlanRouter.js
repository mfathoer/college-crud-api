const studyPlanController = require('./studyPlanController');
const express = require('express');
const router = express.Router();

router.get('/', studyPlanController.getAllStudyPlans);
router.get('/:code', studyPlanController.getStudyPlan);
router.post('/', studyPlanController.insertStudyPlan);
router.put('/:code', studyPlanController.updateStudyPlan);
router.delete('/:code', studyPlanController.deleteStudyPlan);

module.exports = router;
