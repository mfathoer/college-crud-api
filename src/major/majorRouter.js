const majorController = require('./majorController');
const express = require('express');
const router = express.Router();

router.get('/', majorController.getAllMajors);
router.get('/:code', majorController.getMajorByCode);
router.get('/:name/subjects', majorController.getAllSubjectsInMajor);
router.post('/', majorController.insertMajor);
router.put('/:code', majorController.updateMajor);
router.delete('/:name', majorController.deleteMajorByName);

module.exports = router;