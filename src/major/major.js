const db = require('../services/database');

class MajorModel {
    
    static getAllMajors = (callback) => {
        const query = "SELECT * FROM major";

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static getMajorByCode = (code, callback) => {
        const query = `SELECT * FROM major WHERE code=${code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static insertMajor = (name, callback) => {
        const query = `INSERT INTO major (name) VALUES ("${name}")`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static deleteMajorByName = (name, callback) => {
        const query = `DELETE FROM major WHERE name="${name}"`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static updateMajor = (major, callback) => {
        const query = `UPDATE major SET name="${major.name}" WHERE code=${major.code}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static getAllSubjectsInMajor = (name, callback) => {
        const query = `SELECT subject.name FROM subject JOIN major ON subject.major_code = major.code WHERE major.name ="${name}";`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    }
}

module.exports = MajorModel;