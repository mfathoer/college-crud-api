const MajorModel = require('./major');

const getAllMajors = (req, res) => {

    MajorModel.getAllMajors((error, results) => {
        const responseData = {};
        
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = error;
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["data"] = results;
        }

        res.send(JSON.stringify(responseData));
    });
};

const getMajorByCode = (req, res) => {
    const responseData = {};

    const code = req.params.code;
     // validate request param
    if (code == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(code)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter: code";

        res.send(JSON.stringify(responseData));
        return;
    }

    MajorModel.getMajorByCode(code, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "error while retrieving data from database";
        } else {
            if (results.length) {
                res.statusCode = 200;
                responseData["status"] = "Success";
                responseData["data"] = results;
            } else {
                res.statusCode = 404;
                responseData["status"] = "Failed";
                responseData["message"] = "data not found"
            }
        }

        res.send(JSON.stringify(responseData));
    });
};

const insertMajor = (req, res) => {
    const responseData = {};
    const { name } = req.body;
    // validate body request
    if (name == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing value: name";

        res.send(JSON.stringify(responseData));
        return;
    } else if (!(typeof name === 'string')) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid value: name";

        res.send(JSON.stringify(responseData));
        return;
    }

    MajorModel.insertMajor(name, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "insert major failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "new major inserted successfully";
        }
        res.send(JSON.stringify(responseData));
    });
};

const updateMajor = (req, res) => {
    const responseData = {};

    const {code} = req.params;
    const { name } = req.body;

    const major = {
        "code": code,
        "name": name
    };

    MajorModel.updateMajor(major, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = error;
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "update major successfully";
        }
        res.send(JSON.stringify(responseData));
    });
};

const deleteMajorByName = (req, res) => {
    const responseData = {};

    const { name } = req.params;
    // validate request param
    if (name == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: name";
        res.send(JSON.stringify(responseData));
        return;
    } else if (!(typeof name === 'string')) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter name";
        res.send(JSON.stringify(responseData));
        return;
    }

    MajorModel.deleteMajorByName(name, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "delete major failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "major deleted successfully";
        }
        res.send(JSON.stringify(responseData));
    });
};

const getAllSubjectsInMajor = (req,res) => {
    const responseData = {};
    const { name } = req.params;
    // validate request param
    if(name == null){
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter : name";

        res.send(JSON.stringify(responseData));
        return;
    } else if(!(typeof name === 'string')){
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter : name";

        res.send(JSON.stringify(responseData));
        return;
    };

    MajorModel.getAllSubjectsInMajor(name, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = error;
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = results;
        }

        res.send(JSON.stringify(responseData));
    });
}

module.exports = { getAllMajors, updateMajor, getMajorByCode, deleteMajorByName, insertMajor,getAllSubjectsInMajor };