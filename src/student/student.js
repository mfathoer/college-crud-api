const db = require('./../services/database');

class StudentModel {

    static getAllStudents = (callback) => {
        const query = "SELECT * FROM student";

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static getStudentByNim = (nim, callback) => {
        const query = `SELECT * FROM student WHERE nim=${nim}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static insertStudent = (student, callback) => {
        const query = `INSERT INTO student (name, birth_date,birth_place,batch,major_code) VALUES ("${student.name}",
            ${student.birthDate},"${student.birthPlace}",${student.batch},${student.majorCode})`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static deleteStudentByNim = (nim, callback) => {
        const query = `DELETE FROM student WHERE nim=${nim}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static updateStudent = (student, callback) => {
        const query = `UPDATE student SET name="${student.name}",birth_date=${student.birthDate},
            birth_place="${student.birthPlace}",batch=${student.batch},major_code=${student.majorCode}
            WHERE nim=${student.nim}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    };

    static getStudentCredits = (nim, callback) => {
        const query = `SELECT student.nim, student.name, COUNT(subject.credits) FROM student JOIN studyplan ON
            student.nim = studyplan.nim JOIN subject ON studyplan.subject_code=subject.code WHERE student.nim=${nim}`;

        db.query(query, (error, results) => {
            if (error)
                return callback(error, null);
            else
                return callback(null, results);
        });
    }
}

module.exports = StudentModel;