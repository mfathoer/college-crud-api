const StudentModel = require('./student');

const getAllStudents = (req, res) => {

    StudentModel.getAllStudents((error, results) => {
        const responseData = {};

        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = error;
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["data"] = results;
        }

        res.send(JSON.stringify(responseData));
    });
};

const getStudentByNim = (req, res) => {
    const responseData = {};

    const { nim } = req.params;
    // validate request param
    if (nim == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: nim";

        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(nim)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid nim"

        res.send(JSON.stringify(responseData));
        return;
    }

    StudentModel.getStudentByNim(nim, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "error while retrieving data";
        } else {
            if (results.length) {
                res.statusCode = 200;
                responseData["status"] = "Success";
                responseData["data"] = results;
            } else {
                res.statusCode = 404;
                responseData["status"] = "Failed";
                responseData["message"] = "data not found"
            }
        }

        res.send(JSON.stringify(responseData));
    });


};

const insertStudent = (req, res) => {
    const responseData = {};

    const { name, birth_date: birthDate, birth_place: birthPlace, batch, major_code: majorCode } = req.body;

    const student = {
        "name": name,
        "birthDate": birthDate,
        "birthPlace": birthPlace,
        "batch": batch,
        "majorCode": majorCode
    };

    StudentModel.insertStudent(student, (error, results) => {

        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "Save student data failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "Student data saved successfully";
        }

        res.send(JSON.stringify(responseData));
    });
};

const updateStudentByNim = (req, res) => {
    const responseData = {};
    
    const {nim} = req.params;
    const { name, birth_date:birthDate, birth_place:birthPlace, batch, major_code:majorCode } = req.body;

    const student = {
        "nim": nim,
        "name": name,
        "birthDate": birthDate,
        "birthPlace": birthPlace,
        "batch": batch,
        "majorCode": majorCode
    };

    StudentModel.updateStudent(student, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "update student data failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "update student data successfully";
        }

        res.send(JSON.stringify(responseData));
    });
};

const deleteStudentByNim = (req, res) => {
    const responseData = {};

    const { nim } = req.params;
    // validate request param
    if (nim == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: nim";
        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(nim)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter: nim";
        res.send(JSON.stringify(responseData));
        return;
    }

    StudentModel.deleteStudentByNim(nim, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "delete student data failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["message"] = "delete student data successfully";
        }

        res.send(JSON.stringify(responseData));
    });
};

const getStudentCredits = (req, res) => {
    const responseData = {};

    const { nim } = req.body;
    // validate request body
    if (nim == null) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Missing parameter: nim";
        res.send(JSON.stringify(responseData));
        return;
    } else if (isNaN(nim)) {
        res.statusCode = 400;
        responseData["status"] = "Failed";
        responseData["message"] = "Invalid parameter: nim";
        res.send(JSON.stringify(responseData));
        return;
    }

    StudentModel.getStudentCredits(nim, (error, results) => {
        if (error) {
            res.statusCode = 500;
            responseData["status"] = "Failed";
            responseData["message"] = "delete student data failed";
        } else {
            res.statusCode = 200;
            responseData["status"] = "Success";
            responseData["data"] = results;
        }
    });
};


module.exports = { getAllStudents, updateStudentByNim, deleteStudentByNim, getStudentByNim, insertStudent, getStudentCredits };


