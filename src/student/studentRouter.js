const studentController = require('./studentController');
const express = require('express');
const router = express.Router();

router.get('/', studentController.getAllStudents);
router.get('/:nim', studentController.getStudentByNim);
router.get('/:nim/credits',studentController.getStudentCredits);
router.post('/',studentController.insertStudent);
router.put('/:nim', studentController.updateStudentByNim);
router.delete('/:nim', studentController.deleteStudentByNim);

module.exports = router;
